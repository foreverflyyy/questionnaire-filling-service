﻿namespace webapi.Models
{
    public class Requisites
    {
        /// <summary>
        /// Значения вида деятельности
        /// </summary>
        public ActivityValues ActivityValues { get; set; }

        /// <summary>
        /// Список данных привязанных банков
        /// </summary>
        public List<BankValues> CollectionBankValues { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public List<IFormFile> Files { get; set; }
    }

    public class ActivityValues
    {
        public string INN { get; set; }
        public string? OGRNIP { get; set; }
        public string DateOfRegistration { get; set; }
        public bool ThereNoContract { get; set; }
        public string? FullName { get; set; }
        public string? ShortName { get; set; }
        public string? OGRN { get; set; }
    }

    public class BankValues
    {
        public string BIC { get; set; }
        public string NameBankBranch { get; set; }
        public string CorrespondentAccount { get; set; }
        public string PaymentAccount { get; set; }
    }
}

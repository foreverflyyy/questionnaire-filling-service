﻿namespace webapi.Models.Dto
{
    public class CreateNewRequisite
    {
        /// <summary>
        /// Значения вида деятельности
        /// </summary>
        public ActivityValues ActivityValues { get; set; }

        /// <summary>
        /// Список данных привязанных банков
        /// </summary>
        public BankValues BankValues { get; set; }
    }
}

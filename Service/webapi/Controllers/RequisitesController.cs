﻿using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Text.Json;
using webapi.Models;
using webapi.Models.Dto;

namespace webapi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RequisitesController : Controller
    {
        private static List<Requisites> mCollectionRequisites = new List<Requisites>();

        private readonly ILogger<RequisitesController> _logger;
        private readonly HttpClient mHttpClient;

        public RequisitesController(ILogger<RequisitesController> logger, HttpClient httpClient)
        {
            _logger = logger;
            mHttpClient = httpClient;
        }

        [HttpGet("checkWork")]
        public IActionResult CheckWork()
        {
            return Ok();
        }

        [HttpPost("createRequisite")]
        public IActionResult CreateRequisites()
        {
            var files = HttpContext.Request.Form.Files.ToList();

            var valuesJson = HttpContext.Request.Form["data"].ToString();
            var valuesRequisite = JsonSerializer.Deserialize<CreateNewRequisite>(valuesJson) ?? new CreateNewRequisite();

            var existedItemActivity = mCollectionRequisites.FirstOrDefault(x => x.ActivityValues.INN == valuesRequisite.ActivityValues.INN);

            if (existedItemActivity != null)
            {
                existedItemActivity.CollectionBankValues.Add(valuesRequisite.BankValues);
            }
            else
            {
                mCollectionRequisites.Add(new Requisites 
                { 
                    Files = files, 
                    ActivityValues = valuesRequisite.ActivityValues,
                    CollectionBankValues = new List<BankValues> { valuesRequisite.BankValues }
                });
            }

            return Ok();
        }

        [HttpGet("getDataByInn")]
        public async Task<string> GetDataByInn([FromQuery(Name = "Inn")] string Inn)
        {
            _logger.LogInformation("Inn: " + Inn);

            var jsonContent = new StringContent (
                JsonSerializer.Serialize(
                    new
                        {
                            query = Inn,
                            count = 1
                        }
                    ),
                Encoding.UTF8,
                "application/json"
            );

            var response = await mHttpClient.PostAsync("https://www.tinkoff.ru/api/common/dadata/suggestions/api/4_1/rs/suggest/party?appName=company-pages", jsonContent);
            var contents = await response.Content.ReadAsStringAsync();
            return contents;
        }
    }
}
